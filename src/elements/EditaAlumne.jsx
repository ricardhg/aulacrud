import { useState, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate, useParams } from 'react-router-dom';


const APIURL = 'http://api1.ricardhernandez.com/api';


function EditaAlumne() {

    const parametros = useParams();

    const ALUMNE = parametros.id;

    const [nom, setNom] = useState("");
    const [email, setEmail] = useState("");
    const [volver, setVolver] = useState(false);

    //lectura datos antes de editar
    useEffect(() => {
   
        fetch(APIURL + '/alumnes/' + ALUMNE)
          .then(x => x.json())
          .then(data =>{
            setNom(data[0].nom);
            setEmail(data[0].email);
              
          })
          .catch(err => { 
            setError(true);
            console.log(err);
          });
    
      }, []);


    function enviaAlumno(e){
        e.preventDefault();

        const alumno = {
            "nom": nom,
            "email": email
        }

        const opciones = {
            "method": "PATCH",
            "body": JSON.stringify(alumno),
            headers: {"Content-Type": "application/json"}
        }
        const url = APIURL+"/alumnes/"+ALUMNE;

       fetch(url, opciones)
       .then(x => x.json())
       .then(x => setVolver(true))
       .catch(x => console.log(x))

    }


    if (volver){
        return <Navigate to="/alumnes" />;
    }

    return (
        <>
            <h1>Edita Alumne...</h1>
            <Row>
                <Col xs="8">
                    <Form onSubmit={enviaAlumno}>
                        <Form.Group className="mb-3" controlId="formnom">
                            <Form.Label>Nom</Form.Label>
                            <Form.Control value={nom} onInput={(e) => setNom(e.target.value)} type="text" placeholder="Entra nom" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formemail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control value={email} onInput={(e) => setEmail(e.target.value)} type="email" placeholder="Entra email" />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Desar
                        </Button>
                    </Form>
                </Col>
            </Row>

        </>
    );
}

export default EditaAlumne;
