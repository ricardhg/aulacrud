import { useState, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate, useParams } from 'react-router-dom';


const APIURL = 'http://api1.ricardhernandez.com/api';


function BorraAlumne() {
    // useParams() devuelve los parámetros recibidos en la URL
    // ej) /alumnes/borra/33, recibirá un objeto {id: 33}
    const parametros = useParams();

    const ALUMNE = parametros.id;

    const [nom, setNom] = useState("");
    const [volver, setVolver] = useState(false);

    //lectura datos antes de borrar
    //necesitamos el nombre del alumno para preguntar si se quiere borrar
    useEffect(() => {
        // la API requiere una consulta tipo GET a /api/alumnes/33 para devolver el alumno 33 
        fetch(APIURL + '/alumnes/' + ALUMNE)
          .then(x => x.json())
          .then(data =>{
            //los datos devueltos por esta API son un array de un solo elemento
            //por ello hacemos data[0] (primer elemento del array), y luego leemos el nombre
            setNom(data[0].nom);
          })
          .catch(err => { 
            console.log(err);
          });
    
      }, []);


    function borrar(){
        const opciones = {
            "method": "DELETE"
        }
        const url = APIURL+"/alumnes/"+ALUMNE;
        // llamamos a la url de la api /api/alumnes/33 con DELETE, para borrar alumno id=33
       fetch(url, opciones)
       .then(x => x.json())
       //tras borrar, establecemos volver a true para volver a listado
       .then(x => setVolver(true))
       .catch(x => console.log(x))
    }

    // volvemos al listado sin borrar
    function volverListado(){
        setVolver(true)
    }


    // si volver es true, saltamos a /alumnes
    if (volver){
        return <Navigate to="/alumnes" />;
    }

    return (
        <>
            <h1>Borrar Alumno</h1>
            <Row>
                <Col xs="8">
                    <h3>Seguro que quieres borrar al alumno: {nom}?</h3>
                        <Button variant="danger" onClick={borrar}>
                            SI
                        </Button>
                        {' '}
                        <Button variant="primary" onClick={volverListado}>
                            NO
                        </Button>

                </Col>
            </Row>

        </>
    );
}

export default BorraAlumne;
