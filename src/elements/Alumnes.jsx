import { useState, useEffect } from 'react';
import { Row, Col, Table, Button } from 'react-bootstrap';
import EditaAlumne from './EditaAlumne';
import {Navigate} from 'react-router-dom';

const APIURL = 'http://api1.ricardhernandez.com/api';

function Alumnes() {

  const [dades, setDades] = useState(
    []
  );
  const [error, setError] = useState(false);
  const [cargaEditor, setCargaEditor] = useState(false);
  const [cargaBorrar, setCargaBorrar] = useState(false);

  function editaAlumne(id){
    console.log("editando "+id);
    setCargaEditor(id);
  }

  function borraAlumne(id){
    console.log("editando "+id);
    setCargaBorrar(id);
  }

  function convierteJSON(x) {
    return x.json();
  }

  useEffect(() => {
   
    fetch(APIURL + '/alumnes')
      .then(convierteJSON)
      .then(data => setDades(data))
      .catch(err => { 
        setError(true);
        console.log(err);
      });

  }, []);

  //creamos la variable que contendrá las filas que se mostrarán en la tabla, a partir de dades
  let rows = [];
  //solo si dades contiene algo, entramos en bucle para llenar las filas
  if (dades){
    rows=dades.map((e, idx) => (
      <tr key={idx}>
        <td>{e.id}</td>
        <td>{e.nom}</td>
        <td>{e.email}</td>
        <td><Button onClick={()=>editaAlumne(e.id)}>Editar</Button></td>
        <td><Button onClick={()=>borraAlumne(e.id)}>Borrar</Button></td>
      </tr>
    ))
  }

  if (cargaEditor){
    return <Navigate to={"/alumnes/edit/"+cargaEditor} />;
  }

  if (cargaBorrar){
    return <Navigate to={"/alumnes/borra/"+cargaBorrar} />;
  }


  return (
    <>
      <h2>Alumnes</h2>
      <Row>
        <Table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Nom</th>
              <th>Email</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </Table>
      </Row>
    </>

  );
}





export default Alumnes;