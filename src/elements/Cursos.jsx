import { useState, useEffect } from 'react';
import { Row, Col, Table } from 'react-bootstrap';

const APIURL = 'http://api1.ricardhernandez.com/api';

function Cursos() {

  const [dades, setDades] = useState(
    []
  );
  const [error, setError] = useState(false);

  useEffect(() => {
    fetch(APIURL + '/cursos')
      .then(results => results.json())
      .then(data => setDades(data))
      .catch(err => setError(true));
  }, [])

  let rows = [];
  if (dades){
    rows=dades.map((e, idx) => (
      <tr key={idx}>
        <td>{e.id}</td>
        <td>{e.nom}</td>
      </tr>
    ))
  }

  return (
    <>
      <h2>Cursos</h2>
      <Row>
        <Table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Nom</th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </Table>
      </Row>
    </>

  );
}





export default Cursos;